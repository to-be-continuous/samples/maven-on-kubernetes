#!/bin/sh

set -e

echo "[burger-maker] cleanup script: delete all objects with label app=$environment_name"
kubectl delete all,pvc,secret,ingress -l "app=$environment_name"
