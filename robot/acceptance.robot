*** Settings ***
Library               Collections
Library               RequestsLibrary

*** Test Cases ***
Order Burger
    Create Session    burger            ${BASE_URL}
    ${resp}=          Delete request    burger                                       /api/burgers/cheeseburger
    Request Should Be Successful        ${resp}
    Dictionary Should Contain Key       ${resp.json()}                               id
    Should Be Equal                     ${resp.json()['recipe']}                     cheeseburger
    Dictionary Should Contain Key       ${resp.json()}                               ingredients
    Length Should Be                    ${resp.json()['ingredients']}                7
    Should Be Equal                     ${resp.json()['ingredients'][0]['name']}     bun
    Should Be Equal                     ${resp.json()['ingredients'][1]['name']}     red onion
    Should Be Equal                     ${resp.json()['ingredients'][2]['name']}     tomato
    Should Be Equal                     ${resp.json()['ingredients'][3]['name']}     ketchup
    Should Be Equal                     ${resp.json()['ingredients'][4]['name']}     salad
    Should Be Equal                     ${resp.json()['ingredients'][5]['name']}     cheddar
    Should Be Equal                     ${resp.json()['ingredients'][6]['name']}     steak

List Purchases
    Create Session    burger            ${BASE_URL}
    ${resp}=          Get request       burger             /api/purchases
    Request Should Be Successful        ${resp}
    Dictionary Should Contain Key       ${resp.json()}     elements

Check The Menu
    Create Session    burger            ${BASE_URL}
    ${resp}=          Get request       burger                /api/recipes
    Request Should Be Successful        ${resp}
    Length Should Be                    ${resp.json()}        7
    Should Be Equal                     ${resp.json()[0]}     bacon
    Should Be Equal                     ${resp.json()[1]}     cheeseburger
    Should Be Equal                     ${resp.json()[2]}     farmer
    Should Be Equal                     ${resp.json()[3]}     fish
    Should Be Equal                     ${resp.json()[4]}     frenchie
    Should Be Equal                     ${resp.json()[5]}     unreliable
    Should Be Equal                     ${resp.json()[6]}     veggie
