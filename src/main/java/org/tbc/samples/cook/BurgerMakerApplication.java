package org.tbc.samples.cook;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;

import jakarta.ws.rs.core.Application;

@OpenAPIDefinition(info = @Info(title = "burger-maker API", description = "Documentation burger-maker API v1.2", version = "1.2"))
public class BurgerMakerApplication extends Application {
}
