package org.tbc.samples.cook.web;

import java.net.URI;

import org.eclipse.microprofile.openapi.annotations.Operation;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

/**
 * Redirects / to Swagger UI
 */
@Path("/")
public class HomeController {
    @GET
    @Operation(hidden = true)
    public Response redirectToSwaggerUi() {
        return Response.temporaryRedirect(URI.create("/swagger-ui/")).build();
    }
}
