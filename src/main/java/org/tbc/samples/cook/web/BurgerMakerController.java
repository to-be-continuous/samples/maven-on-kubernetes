package org.tbc.samples.cook.web;

import java.time.Instant;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.tbc.samples.cook.domain.Burger;
import org.tbc.samples.cook.domain.Purchase;
import org.tbc.samples.cook.service.ChefService;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/api")
@Tag(name = "Burger maker API", description = "This API delivers burgers.")
@Produces(MediaType.APPLICATION_JSON)
public class BurgerMakerController {

    @Inject
    protected ChefService chefService;

    @Operation(description = "Displays the menu")
    @APIResponse(responseCode = "200", description = "Burger recipes successfully displayed")
    @GET()
    @Path("/recipes")
    public Iterable<String> gimeTheMenu() {
        return chefService.recipes();
    }

    @Operation(description = "Displays the ingredients of a given recipe")
    @APIResponse(responseCode = "200", description = "Recipe ingredients successfully displayed")
    @APIResponse(responseCode = "404", description = "Requested recipe is unknown")
    @GET()
    @Path("/recipes/{recipe}")
    public Iterable<String> gimeTheIngredients(
            @Parameter(description = "name of the recipe", example = "cheeseburger") @PathParam("recipe") String recipe) {
        return chefService.ingredients(recipe);
    }

    @Operation(description = "Deliver a burger and logs the purchase")
    @APIResponse(responseCode = "200", description = "Burger successfully delivered")
    @APIResponse(responseCode = "404", description = "Requested recipe is unknown")
    @APIResponse(responseCode = "503", description = "No burger available")
    @DELETE()
    @Transactional
    @Path("/burgers/{recipe}")
    public Burger prepareMe(
            @Parameter(description = "name of the recipe ('any' to get surprised)", example = "any") @PathParam("recipe") String recipe) {
        Burger burger = chefService.prepareBurger(recipe);
        Purchase purchase = new Purchase(Instant.now(), burger);
        purchase.persist();
        return burger;
    }

    @Operation(description = "Returns purchase logs, sorted by date (descending)")
    @GET()
    @Path("/purchases")
    public PaginatedResponse<Purchase> findPurchases(
            @Parameter(description = "pagination page number (zero-based)") @QueryParam("page") @DefaultValue("0") int page,
            @Parameter(description = "pagination page size") @QueryParam("size") @DefaultValue("10") int size,
            @Parameter(description = "filter by recipe") @QueryParam("recipe") String recipe) {
        if (recipe == null) {
            return new PaginatedResponse<>(Purchase.findAll().page(page, size));
        } else {
            return new PaginatedResponse<>(Purchase.find("burger.recipe", recipe).page(page, size));
        }
    }
}
