package org.tbc.samples.cook.domain;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "An ingredient")
@Entity(name = "ingredients")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ingredient {
    @Schema(description = "Ingredient's id")
    @Id
    private String id;

    @Schema(description = "Ingredient's name. E.g.: bun, cheese...")
    @Column
    private String name;
}
